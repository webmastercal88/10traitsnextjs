import Header from '../components/Header'
import Footer from '../components/Footer'

export default ({ children }) => (

<div className="main back-color">  
<Header></Header>
    { children }
<Footer />
</div>
)