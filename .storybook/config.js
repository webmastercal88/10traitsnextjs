import { configure } from '@storybook/react'
import '../src/components/styles/index.css'

// automatically import all files ending in *.stories.js
/*const req = require.context('../src', true, /(\.|\/).stories.js$/)
function loadStories () {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)*/

function importAll (req) {
  req.keys().forEach(filename => req(filename))
}

function loadStories () {
  let req
  req = require.context("../src",true,/(\.|\/)stories\.js$/)
  importAll(req)
}
configure(loadStories, module)