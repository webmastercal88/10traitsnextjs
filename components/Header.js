import React from "react"
import Head from "next/head"
import Link from "next/link"
import '../styles/style.scss'
import { Nav, NavItem } from "reactstrap";

export default () => (
      <div>
        <Head>
          <title>"10TRAITS ASSESSMENT"</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossOrigin="anonymous"
          />
          <link 
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" 
            />
            
          <link 
          rel="stylesheet" 
          href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" 
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" 
          crossOrigin="anonymous"
          />

            <script src="https://js.stripe.com/v3" />

            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossOrigin="anonymous"/>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossOrigin="anonymous"/>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossOrigin="anonymous"/>
        </Head>
          <Nav className="navbar navbar-expand-lg bg-white bar-style p-3">
            <div className="container">
          <NavItem>
              <Link href="/10Traits">
                <a className="navbar-brand blue-color"><h2><font className="font-weight-bold">10</font>TRAITS</h2></a>
              </Link>
            </NavItem>
  <button className="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span className="fa fa-align-justify blue-color "></span>
  </button>
  <div className="d-flex">
              <div className="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
            <NavItem>
              <Link href="/10Traits">
                <a className="nav-link blue-color font-weight-medium flink">ABOUT US</a>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/10Traits">
                <a className="nav-link blue-color font-weight-medium flink"><u>OUR ADVISERS</u></a>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/10Traits">
                <a className="nav-link blue-color font-weight-medium flink">HOW 10TRAITS WORKS</a>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/10Traits">
                <a className="nav-link blue-color font-weight-medium top5 h5">303-443-3697</a>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/">
                <a className="nav-link blue-color font-weight-medium box-resign flink text-center">Sig In | <u>Register</u></a> 
              </Link>
            </NavItem>
            </div>
            </div>
            </div>
          </Nav>
     
      </div>

    )
