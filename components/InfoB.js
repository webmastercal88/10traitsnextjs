import React from "react";
import Info from '../svgs/information.svg'

export default ({children}) => (
    
    <div className="d-flex justify-content-center top15 h5 info" data-toggle="modal" data-target="#exampleModalCenter">
       { children }
    <div className="content-secondary ml-1">
      <Info />
      </div>
    </div>

)
