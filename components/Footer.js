import Link from 'next/link'

export default () => (

<div className="text-center footer-style"> 
<div className="navbar-brand blue-color"><h3><font className="font-weight-bold">10</font>TRAITS</h3></div>
<p className="text-secondary"><small>Your access and use of this website are subject to our <Link href="/"><a className="blue-color font-weight-medium">Privacy Policy</a></Link>.</small></p>
<p className="text-secondary"><small>In <Link href="/"><a className="blue-color font-weight-medium">10Traits</a></Link> you have 10 powerful traits that are exactly what’s needed,</small>
<div><small>urgently needed, in the world today. When you learn about these traits, study them and reflect on them</small></div>
<div><small>they will manifest as inner power, courage and will to express your full potential.</small></div></p>
<p className="text-secondary"><div><small>© 2019 10Traits LLC, 1035 Pearl Street, Boulder, CO 80302.</small></div></p>
</div>

)