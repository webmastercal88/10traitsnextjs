import React from 'react'
import Base from '../layouts/base'
import Inputs from '../components/Inputs'

const App = () => {

  return (
<Base>  
<div className="container"> 
<div className="row">
  <div className="col-md-8 offset-md-2 bg-white pageLogin-style">

  <div className="blue-color h3">10Traits tool</div>
<div className="text-secondary">You have 10 powerful traits to assess a relationship. Each trait is paired with its opposite.</div>
<div className="text-secondary">You can evaluate just one person or two.</div>
    <Inputs/>
    </div>
    </div>
</div>
</Base>

)
}

export default App
