import { configure } from '@storybook/react';
import '../src/components/styles/index.css';

function importAll (req) {
req.keys().forEach(filename => req(filename));
}

function loadStories () {
  let req
  req = require.context("../src",true,/(\.|\/)stories\.js$/)
  importAll(req)
}
configure(loadStories, module)