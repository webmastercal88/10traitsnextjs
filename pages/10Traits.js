import React, { useState } from 'react'
import Link from "next/link"
import Base from '../layouts/base'
import Squares from '../components/Squares'
import SquaresInv from '../components/SquaresInv'
import InfoB from '../components/InfoB'
import Info from '../svgs/information.svg'

const App = () => {

  const usersData = [
    { id: 1, name: 'Willingness to take risks', top: 'Risk Taker + Competitive', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Avoids Risk', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 2, name: 'Ability to exhibit emotions', top: 'Emotions are Controlled', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Displays Emotions', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 3, name: 'Ability to feel empathy', top: 'Less Empathetic', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Highly Empathetic', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 4, name: 'Preference of small teams over networking', top: 'Works Best in Small Teams', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Prefers Networking', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 5, name: 'Comfort level with touch', top: 'Lesser Tendency to Touch', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Displays Touch Easily', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 6, name: 'Preference of machines over people', top: 'Loves Tools / Tech', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Love of Community', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 7, name: 'Ability to multi-task', top: 'Focused Concentration', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Multi-tasking Master', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 8, name: 'Preference of abstraction over details', top: 'Big Picture Vision', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Detailed Oriented', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 9, name: 'Ability to think logically', top: 'Analytical', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Balance + Prioritize', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
    { id: 10, name: 'Acute stress response', top: 'Fight or Flight', topInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.', down: 'Diplomatic Befriend', downInfo: 'Chooses fighting as the fastest way to resolve a conflict or resolve a dispute. Competitive and may also be aggressive in their deisire to win.' },
  ]

  const [users] = useState(usersData)

  const [count, setCount] = useState(0)

  return (

<Base>  

<div className="container bg-white page-style"> 

<div className="row bg-white top5">
<Link href="/">
  <a className="text-secondary"><u>Assessment</u></a>
  </Link>
  <div className="text-secondary px-2"> <span>&gt;</span> </div>
<Link href="/10Traits">
  <a className="text-secondary"> New 10traits Assessment </a>
  </Link>
</div>

<div className="p-3 blue-color d-flex justify-content-center h4 top15"><font className="font-weight-bold">10TRAITS ASSESSMENT</font></div>

<div className="row justify-content-between top5">
<div className="p-2 info"><div onClick={() => setCount(0)} className={count >= 0 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 1 ? 'fa fa-check top6' : ''}></span><div className={count === 1  ? 'invisible' : ''}>1</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(1)} className={count >= 1 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 2 ? 'fa fa-check top6' : ''}></span><div className={count === 2 ? 'invisible' : ''}>2</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(2)} className={count >= 2 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 3 ? 'fa fa-check top6' : ''}></span><div className={count === 2 ? 'visible' : ''}>3</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(3)} className={count >= 3 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 4 ? 'fa fa-check top6' : ''}></span><div className={count === 3 ? 'visible' : ''}>4</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(4)} className={count >= 4 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 5 ? 'fa fa-check top6' : ''}></span><div className={count === 4 ? 'visible' : ''}>5</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(5)} className={count >= 5 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 6 ? 'fa fa-check top6' : ''}></span><div className={count === 5 ? 'visible' : ''}>6</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(6)} className={count >= 6 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 7 ? 'fa fa-check top6' : ''}></span><div className={count === 6 ? 'visible' : ''}>7</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(7)} className={count >= 7 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 8 ? 'fa fa-check top6' : ''}></span><div className={count === 7 ? 'visible' : ''}>8</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(8)} className={count >= 8 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 9 ? 'fa fa-check top6' : ''}></span><div className={count === 8 ? 'visible' : ''}>9</div></div></div></div>
<div className="p-2 info"><div onClick={() => setCount(9)} className={count >= 9 ? 'circleA-text' : 'circle-text'}><div><span className={count >= 10 ? 'fa fa-check top6' : ''}></span><div className={count === 9 ? 'visible' : ''}>10</div></div></div></div>
</div>

<div className="modal fade" id="exampleModalCenterTop" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div className="modal-dialog modal-dialog-centered" role="document">
    <div className="modal-content">   
      <div className="padd40"> 
      <InfoB>{users[count].top}</InfoB>
      <div className="d-flex justify-content-center text-center">
      {users[count].topInfo}
        </div>
        <div className="d-flex justify-content-center top15">
        <button type="button" className="btn btn-primary btn-lg buttonW" data-dismiss="modal">OK</button>
        </div>
    </div>
  </div>
</div>
</div>

<div className="p-3 blue-color d-flex justify-content-center font-weight-medium top15 h5">{users[count].name}</div>

<div className="row">
<div className="col-md-4 col-sm-12">
  <Squares></Squares>
  </div>
  <div className="col-md-4 col-sm-12">
<div className="d-flex justify-content-center top15 h5 info" data-toggle="modal" data-target="#exampleModalCenterTop">
    {users[count].top}
    <div className="content-secondary ml-1">
    <Info />
      </div>
    </div>
  </div>
  <div className="col-md-4 col-sm-12">
  <SquaresInv></SquaresInv>
  </div>
</div>

  <div className="d-flex justify-content-center h5 info" data-toggle="modal" data-target="#exampleModalCenterDown">
    {users[count].down}
    <div className="content-secondary ml-1">
    <Info />
      </div>
    </div>

  <div className="modal fade" id="exampleModalCenterDown" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div className="modal-dialog modal-dialog-centered" role="document">
    <div className="modal-content">
      <div className="padd40"> 
      <InfoB>{users[count].down}</InfoB>
      <div className="d-flex justify-content-center text-center">
      {users[count].downInfo}
        </div>
        <div className="d-flex justify-content-center top15">
        <button type="button" className="btn btn-primary btn-lg buttonW" data-dismiss="modal">OK</button>
        </div>
    </div>
  </div>
</div>
</div>


<div className="row justify-content-between top15">
  <div className="col-3 d-flex">
  <button onClick={() => setCount(count - 1)} className={count === 0 ? 'btn btn-primary btn-lg buttonW invisible' : 'btn btn-primary btn-lg buttonW'}>Previous</button>
  </div>
<div className="col-4 d-flex justify-content-center">
  </div>
  <div className="col-3 d-flex flex-row-reverse">
  <button onClick={() => setCount(count + 1)} className={count === 9 ? 'btn btn-primary btn-lg buttonW invisible' : 'btn btn-primary btn-lg buttonW'}>Next</button>
  </div>
</div>

</div>

</Base>

)
}

export default App
