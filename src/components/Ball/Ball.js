import PropTypes from "prop-types"

export function Ball({ ball: { title, state } }) {
  
    return (
   
         <div className="p-2"><div className=
         {(state==1 || state==2) ? ("circleA-text") : ("circle-text")}
         ><div>
         {(state==1) ? ( <span className="fa fa-check"> </span>) : ("")}
         {(state!=1 ) ? (title) : ("")}
         </div></div></div>
        
    );
  }
  Ball.propTypes = {
    ball: PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      state: PropTypes.string.isRequired,
    }),
    onArchiveTask: PropTypes.func,
    onPinTask: PropTypes.func,
  };

  export default Ball;