import React from 'react';
import { storiesOf } from '@storybook/react';
import Ball from "./Ball"
import {action } from "@storybook/addon-actions"

export function createBall(attrs) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    title: 'Test Task',
    state: '00',
    updatedAt: Date.now(),
    ...attrs,
  };
}

export const actions = {
  onPinTask: action('onPinTask'),
  onArchiveTask: action('onArchiveTask'),
};

storiesOf('Ball', module)
  .add(' Ball1', () => <Ball ball={createBall({ title: '4', state: '1' })} {...actions}  />)
  .add(' Ball2', () => <Ball ball={createBall({ title: '5', state: '2' })} {...actions}  />)
  .add(' Ball3', () => <Ball ball={createBall({ title: '6', state: '3' })} {...actions}  />)