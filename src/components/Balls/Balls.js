import React, { useState } from 'react';
import Ball from "../Ball/Ball"
import PropTypes from "prop-types"

export function createBall(attrs) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    title: 'Test Task',
    state: '00',
    updatedAt: Date.now(),
    attrs,
  };
}

export  function Balls({ balls, onArchiveTask, onPinTask,onClick1 }) {
    const [pos, setPosition] = useState(0);  
  
   const events = {
    onPinTask,
    onArchiveTask,
    onClick1,
  };
  
  var a=0;
    return (

      <div >
      {balls.map(ball=> <Ball key={ball.id} ball={createBall({title:a++, state: (a==pos) ? (1) : ((a<pos) ? (2 ) : (3))     })} {...events} />)}
      
        <button onClick={() => setPosition(pos + 1)} >
        Next
      </button>
      <button onClick={() => setPosition(pos - 1)}>
        Previous
      </button>
        </div>      

    );
  }

  Balls.propTypes = {
    loading: PropTypes.bool,
    balls: PropTypes.arrayOf(Ball.propTypes.ball).isRequired,
    onPinTask: PropTypes.func.isRequired,
    onArchiveTask: PropTypes.func.isRequired,
    onClick1: PropTypes.func.isRequired,
  };
  
  Balls.defaultProps = {
    loading: false,
  };
  
  export default Balls;
