import React, { useState } from 'react';
import Link from "next/link"

 export default function Inputs() {

  const [count, setCount] = useState("1");
  const [change, setChange] = useState("0");

  return (
    
 <div>
    <form>
<label className="text-secondary top25">Assessment Type</label>

<div className="dropdown show buttonW">
   {(count==1)
?(
<a className="btn btn-secondary dropdown-toggle bg-white text-secondary top5" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Relationship 
</a>
)
:(
<a className="btn btn-secondary dropdown-toggle bg-white text-secondary top5" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Self
</a>)}

  <div className="dropdown-menu" aria-labelledby="dropdownMenuLink" >
  <a className="dropdown-item info" onClick={() => setCount(1)}>Relationship</a>
  <a className="dropdown-item info" onClick={() => setCount(count + 2)}>Self</a>
  </div>
</div>

<div className="row">
<div className="col-md-6 col-sm-12">
{(count==1)
?(
<div className="top25">
<input type="text" onChange={() => setChange(1)} className="form-control border-top-0 border-right-0 border-left-0" id="validationDefault01" placeholder="Name of Assessed Person" required />
<input type="text" onChange={() => setChange(1)} className="form-control  top15 border-top-0 border-right-0 border-left-0" id="validationDefault02" placeholder="Name of Other Person" required />
</div>
)
:(
<div className="top25">
<input type="text" onChange={() => setChange(1)} className="form-control border-top-0 border-right-0 border-left-0" id="validationDefault01" placeholder="Name of Assessed Person" required />
</div>)}
</div>
</div>

<div className="col-12 d-flex flex-row-reverse top40">
<Link href={ change===1 ? '/10Traits' : ''}>
  <button className={ change===1 ? 'btn btn-primary buttonW' :'btn btn-secondary disabled buttonW'}>Begin</button>
  </Link>
  </div>
  </form>
</div>

  );
}