import React, { useState } from 'react'
import Slider from 'react-rangeslider'

export function createBall(attrs) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    title: 'Test Task',
    state: '00',
    updatedAt: Date.now(),
    attrs,
  };
}

export default function Squares() {

    const [val, updateText] = useState(5);
  
    return (

<div className="d-flex flex-row">
  
<div className="col-md-10 col-sm-10 col-10 text-center ">

<div className="p-3 blue-color font-weight-medium text-center"><h5>Maurice</h5></div>
<div className="square-style">
<h5 onClick={() => updateText(9)}className= {(val==9) ? ( "p-3 green-background text-white bu-style" ) : ( "p-3 green-color bu2-style border-bottom font-weight-medium") }>Extreme</h5>      
<h5 onClick={() => updateText(8)}className= {(val==8) ? ( "p-3 green-background text-white bu-style" ) : ( "p-3 green-color bu2-style border-bottom font-weight-medium") }>Very Strong</h5>
<h5 onClick={() => updateText(7)}className= {(val==7) ? ( "p-3 green-background text-white bu-style" ) : ( "p-3 green-color bu2-style border-bottom font-weight-medium") }>Strong</h5>
<h5 onClick={() => updateText(6)}className= {(val==6) ? ( "p-3 green-background text-white bu-style" ) : ( "p-3 green-color bu2-style border-bottom font-weight-medium") }>Moderate</h5>

<h5 onClick={() => updateText(5)}className="p-3 yellow-background yellow-color font-weight-medium">Center</h5>

<h5 onClick={() => updateText(4)}className={(val==4) ? ( "p-3 blue-background text-white bu-style" ) : ( "p-3 blue-color bu2-style border-bottom font-weight-medium") }>Moderate</h5>
<h5 onClick={() => updateText(3)}className={(val==3) ? ( "p-3 blue-background text-white bu-style" ) : ( "p-3 blue-color bu2-style border-bottom font-weight-medium") }>Strong</h5>
<h5 onClick={() => updateText(2)}className={(val==2) ? ( "p-3 blue-background text-white bu-style" ) : ( "p-3 blue-color bu2-style border-bottom font-weight-medium") }>Very Strong</h5>
<h5 onClick={() => updateText(1)} className={(val==1) ? ( "p-3 blue-background text-white bu-style" ) : ( "p-3 blue-color bu2-style border-bottom font-weight-medium") }>Extreme</h5>
</div>
</div>

<div className="col-md-2 col-sm-2 col-2 d-flex flex-row-reverse">
        <div className='slider-group'>
          <div className='slider-vertical'>
            <Slider
              min={1}
              max={9}
              value={val}
              orientation='vertical'
              onChange={(val) => updateText(val)}
            />
        </div>
      </div>
</div>  
      </div>

    )
  }
