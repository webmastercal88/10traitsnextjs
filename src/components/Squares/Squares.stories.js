import React from 'react';
import { storiesOf } from '@storybook/react';
import Squares from "./Squares"
import {action } from "@storybook/addon-actions"

export const defaultTasks = [
  createBall({ title: '1', state: '1' }),
  createBall({ title: '2', state: '1'  }),
  createBall({ title: '3', state: '1' }),
  createBall({ title: '4', state: '1'  }),
  createBall({ title: '5', state: '2'  }),
  createBall({ title: '6', state: '3'  }),
  createBall({ title: '7', state: '3'  }),
  createBall({ title: '8', state: '3'  }),
  createBall({ title: '9', state: '3' }),
  createBall({ title: '10', state: '3'  }),

];


export function createBall(attrs) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    title: 'Test Task',
    state: '00',
    updatedAt: Date.now(),
    ...attrs,
  };
}

export const actions = {
  onPinTask: action('onPinTask'),
  onArchiveTask: action('onArchiveTask'),
};

storiesOf('Squares', module)
  .add(' Squares', () => <Squares Squares={defaultTasks}  {...actions}/>)