import React, { useEffect,useState } from 'react';
import Slider from 'react-rangeslider'
 
 export const mybuttons = [
  createMyButton({ state: 'TASK_INBOX1',valor: '5' }),
  createMyButton({ state: 'TASK_INBOX2' ,valor: '4' }),
  createMyButton({ state: 'TASK_INBOX3' ,valor: '3' }),
  createMyButton({ state: 'TASK_INBOX4' ,valor: '2' }),
  createMyButton({ state: 'TASK_INBOX5' ,valor: '1' }),
  createMyButton({ state: 'TASK_INBOX6' ,valor: '0' }),
];

export function createMyButton(attrs) {
  return {
    id: Math.round(Math.random() * 1000000).toString(),
    title: 'Test Button',
    state: 'TASK_INBOX',
    updatedAt: Date.now(),
    valor:'0',
    ...attrs,
  };
}

export default function MyAll1 () {

    const [count, setCount] = useState(0);
    const [val, updateText] = useState(1);
  
          return (

<div>
  
    <div className="list-items">
      {mybuttons.map(mybutton => <button key={mybutton.id}  mybutton={mybutton} onClick={() => this.handleClick(mybutton.valor)}>
      Boton {mybutton.valor} {(val==mybutton.valor) ? ( "Pressed" ) : ( "")}
      </button>)}
    </div>

    <div className='slider orientation-reversed'>
        <div className='slider-group'>
          <div className='slider-vertical'>
            <Slider
              min={0}
              max={5}
              value={val}
              orientation='vertical'
              onChange={(val) => updateText(val)}
            />
          </div>
        </div>
      </div>
    
      </div>
        )
        
    }